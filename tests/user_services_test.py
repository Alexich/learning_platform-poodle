import unittest
from unittest.mock import Mock, patch
from fastapi import HTTPException
from app.services import users_services

from data.models import User, DBuser
from data.utilities import password_hash

# Not working yet.


class UserServices_Should(unittest.TestCase):
    def setUp(self) -> None:
        self.pswrd = password_hash("kalin123")
        self.params = [
            (
                1,
                "kalin",
                self.pswrd,
                "Kalin",
                "Kalinov",
                "+359897122227",
                "fake link",
                False,
                False,
                False,
                False,
            )
        ]

        # self._get_func = lambda id: self.params

    def test_create_user_with_correct_parameters(self):
        baseuser = User.from_query_result(*self.params[0][1:])
        with patch("app.services.users_services.data") as mock_db:
            mock_db.insert_query.return_value = 1
            mock_db.read_query.return_value = self.params

            def _insert_func(x): return 1
            def _get_row_func(x): return self.params

            result = users_services.create_user(baseuser)
            expected = users_services.create_user(
                baseuser, _insert_func, _get_row_func)

            self.assertEqual(result, expected)
