import unittest
from unittest.mock import Mock, patch
from app.services import student_services
from data.models import StudentsAccInfoResponse,StudentsAccInfoChange,StudentCoursesResponse

TEST_TITLE="title"
TEST_DESCRIPTION= "description"
TEST_OBJECTIVE= "objective"
TEST_TEACHER="teacher"
TEST_PREMIUM=True
TEST_SECTIONS=2
class Student_Services_Should_test(unittest.TestCase):
    def setUp(self) -> None:
        self.stud_params = [
            (
                1,
                "Some username",
                "some firstname",
                "some lastname"
            )
        ]

    # def tearDown(self) -> None:
    #     for k, v in vars(self).items():
    #         setattr(self, k, None)


    def test_view_acc_info_with_correctId(self):
        with patch("app.services.student_services.read_query") as mock_db:
            mock_db.return_value = self.stud_params

            result = student_services.view_acc_info(1)
            expected = StudentsAccInfoResponse.from_query_result(*self.stud_params[0])

            self.assertEqual(result, expected)

    def test_view_acc_infoReturns_None(self):
        with patch("app.services.student_services.read_query") as mock_db:
            mock_db.return_value = []

            result = student_services.view_acc_info(1)
            expected = None

            self.assertEqual(result, expected)

    def test_update_returnsUpdate_acc(self):
        with patch('app.services.student_services.update_query') as update_func:
            # Arrange
            update_func.return_value = True
            id=1
            new_info=StudentsAccInfoChange(fname='Pesho', lname='Peshov')
            result=student_services.update_acc(id,new_info)
            self.assertEqual(result, True)

    def test_view_courses_returnsCorrectResponse(self):
        with patch("app.services.student_services.read_query") as mock_db:
            # Arrange
            test_id = 5
            mock_db.return_value = [
                (1, TEST_TITLE, TEST_DESCRIPTION, TEST_OBJECTIVE,TEST_TEACHER,TEST_PREMIUM,TEST_SECTIONS)]

            # Act
            result = list(student_services.view_courses(test_id))
            # Assert
            self.assertEqual(1, len(result))

    def test_get_user_coursesReturns_listWith_courses(self):
        with patch("app.services.student_services.read_query") as mock_db:
            username="fake user"
            user_id=1
            courses_id=2
            mock_db.return_value=[(user_id,courses_id,0)]
            result=student_services.get_user_courses(username)
            expected=[2]
            self.assertEqual(result, expected)

    def test_unsubscribe_from_courseRemovesuser(self):
        with patch("app.services.student_services.update_query") as mock_db:
            course_id=10
            user_id=20
            mock_db.return_value=True
            result=student_services.unsubscribe_from_course(course_id,user_id)
            mock_db.assert_called_once_with(
                'DELETE FROM user_has_courses WHERE courses_id=? and user_id=?',
                (10, 20)
            )
            self.assertEqual(result, True)

    def test_get_id_by_usernameReturns_id(self):
        with patch("app.services.student_services.read_query") as mock_db:
            username="fake username"
            mock_db.return_value=[(1,)]
            result=student_services.get_id_by_username(username)
            expected=1
            self.assertEqual(expected, result)

    def test_check_if_enrolledReturns_True(self):
        with patch("app.services.student_services.read_query") as mock_db:
            course_id=2
            student_id=5
            mock_db.return_value=True
            result=student_services.check_if_enrolled(course_id,student_id)
            expected=True
            self.assertEqual(expected, result)

    def test_check_if_enrolledReturns_False(self):
        with patch("app.services.student_services.read_query") as mock_db:
            course_id=2
            student_id=5
            mock_db.return_value=False
            result=student_services.check_if_enrolled(course_id,student_id)
            expected=False
            self.assertEqual(expected, result)

    def test_review_exists_alreadyReturns_True(self):
        with patch("app.services.student_services.read_query") as mock_db:
            course_id = 2
            user_id = 4
            mock_db.return_value = True
            result=student_services._review_exists_already(course_id,user_id)
            expected=True
            self.assertEqual(expected,result)

    def test_review_exists_alreadyReturns_False(self):
        with patch("app.services.student_services.read_query") as mock_db:
            course_id = 2
            user_id = 4
            mock_db.return_value = False
            result=student_services._review_exists_already(course_id,user_id)
            expected=False
            self.assertEqual(expected,result)



