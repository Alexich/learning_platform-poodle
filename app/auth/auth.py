from fastapi import APIRouter, Depends
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from app.services import auth_services
from data.models import Token

auth_router = APIRouter(tags=["Authentication"])


@auth_router.post("/login", response_model=Token)
def login(login_details: OAuth2PasswordRequestForm = Depends()):
    return auth_services.create_token(login_details)
