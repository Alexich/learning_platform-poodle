from data.data import read_query, insert_query, update_query, query_count
from data.models import PublicCoursesInfo, StudentsCourseResponse
from app.services import teachers_services


def get_public_courses(name: str = None, tag: str = None):
    '''Returns a tuple with all available public courses'''

    sql_query = '''SELECT c.title,c.description,t.tag_name as tags
                             FROM courses c
                             JOIN tags_has_courses p ON c.id=p.courses_id
                             JOIN tags t on t.id=p.tags_id                           
                             WHERE c.is_deactivated=0 AND c.is_premium=0'''

    where_clauses = []
    if name:
        where_clauses.append(f'c.title LIKE "%{name}%"')
    if tag:
        where_clauses.append(f't.tag_name like "%{tag}%"')
    if where_clauses:
        sql_query += ' AND ' + " AND ".join(where_clauses)

    return (PublicCoursesInfo.from_query_result(*row) for row in read_query(sql_query))


def unsubscribe_from_course(course_id: int, id: int):
    '''Removes the subscription of a student from DB'''
    data = update_query(
        'DELETE FROM user_has_courses WHERE courses_id=? and user_id=?', (course_id, id))

    return data


def subscribe_to_course(course_id: int, user_id: int):
    '''Adds a subscription to DB which later on must be approved by the owner of the course'''
    if user_id is None or course_id is None:
        return None
    sql = "INSERT INTO user_has_courses (user_id, courses_id, approved) VALUES (?, ?, 0)"
    sql_params = (user_id, course_id)
    try:
        result = update_query(sql, sql_params)
    except:
        return None
    if not result:
        return None
    return result


def check_premium_course_avaiability(course_id: int, student_id: int):
    '''Checks if the student could subscribe to a premium course.
        Returns true if student can subscribe. False otherwise'''
    course = teachers_services.view_course(course_id)
    if not course:
        return
    if course.is_premium:
        premium_courses_left = read_query(
            '''SELECT premium_courses_left from poodle2.user WHERE id=?''', (student_id,))
        if premium_courses_left[0][0] > 0:
            return True
        return False
    return True
