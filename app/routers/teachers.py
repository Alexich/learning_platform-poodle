from fastapi import APIRouter, Depends, HTTPException, Response
from app.services import teachers_services, auth_services
from data.models import CreateCourse, Course, EditSection, TeacherAccInfoResponse,\
    UpdateCourse, TokenInfo, TeacherAccInfoChange, CreateSection
from app.services import student_services

teachers_router = APIRouter(prefix='/teachers')


@teachers_router.get('/info')
def view_teacher_acc(logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    id = logged_user.id
    result = teachers_services.view_acc_info(id)
    if not result:
        raise HTTPException(status_code=404, detail="No info found")
    return result


@teachers_router.put('/profile')
def update_teacher_acc(new_info: TeacherAccInfoChange, logged_user: TokenInfo = Depends(auth_services.get_current_user)):  # tokenhere
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    id = logged_user.id
    teacher_info = teachers_services.view_acc_info(id)
    if not teacher_info or logged_user.username != teacher_info.username:
        raise HTTPException(status_code=403)
    teachers_services.update_acc(id, new_info)
    result = teachers_services.view_acc_info(id)
    return result


@teachers_router.get('/course/{course_id}/enrollments')
def get_enrollments(course_id: int, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    course = teachers_services.view_course(course_id)
    if not course:
        raise HTTPException(status_code=404, detail="Invalid Course id")
    if course.owner != logged_user.username:
        raise HTTPException(
            status_code=401, detail="You must be the owner of the course")
    result = teachers_services.get_enrolled_students(course_id)
    if not result:
        return Response(status_code=200, content='There are no pending enrollments')
    return result


@teachers_router.post('/course/{course_id}/approve/{student_id}')
def approve(course_id: int, student_id: int, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    course = teachers_services.view_course(course_id)
    if not course:
        raise HTTPException(status_code=404, detail="Invalid Course id")
    if course.owner != logged_user.username:
        raise HTTPException(
            status_code=401, detail="You must be the owner of the course")
    user_exist = student_services.view_acc_info(student_id)
    if not user_exist:
        raise HTTPException(status_code=404, detail="Incorrect student id")

    is_approved = teachers_services.approve_students(course, student_id)
    if is_approved:
        return Response(status_code=200,
                        content=f'Student with id: {student_id} approved!')
    else:
        return Response(status_code=200,
                        content=f'Student with id: {student_id} is already enrolled!')
