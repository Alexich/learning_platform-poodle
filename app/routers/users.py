from typing import List, Tuple, Union
from fastapi import status, HTTPException, Depends, APIRouter
from data.models import User, DBuser, Token
from app.services import auth_services
from app.services import users_services
from data.utilities import password_hash


user_router = APIRouter(prefix="/users", tags=["Users"])


@user_router.post("/register", status_code=status.HTTP_201_CREATED, response_model=User)
def create_user(user: User) -> DBuser:
    # hash the password - user.password
    hashed_password = password_hash(user.password)
    user.password = hashed_password
    if user.is_teacher and (user.phone_number == None or user.linked_account == None):
        raise HTTPException(
            status_code=403, detail='You must provide a phone number and account link to register as a teacher')
    db_user = users_services.create_user(user)
    if not db_user:
        raise HTTPException(
            status_code=status.HTTP_417_EXPECTATION_FAILED,
            detail="User already exists",
        )

    return db_user
