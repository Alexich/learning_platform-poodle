INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'admin',
        '$2b$12$SALAPmLR8L81oTFAAp24MOI9XEQ4V6V6cYExjOGgdkDAg1HhK9UXu',
        'admin',
        'admin',
        0,
        1,
        1
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'PPetrov@abv.bg',
        '$2b$12$TceB9TdThGzpqLDg9BrwceKUYRZUkHIWK59r6lGfOx2ZunnqwDRM2',
        'Pesho',
        'Petrov',
        "0897777777",
        0,
        0,
        0
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'GAngelov@gmail.com',
        '$2b$12$y1uotHqHBfDNVVZLZbV6m.5MZA2CSutRUps2gktpsMYxXlFSKmpBW',
        'Georgi',
        'Angelov',
        "0879999999",
        1,
        0,
        0
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        linked_account,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'DNedqlkova@abv.bg',
        '$2b$12$PAToIBmfrfVtaX1RkHHWZuAbO9StYlyKO0hSs.7nwVLkCGEje2ZHa',
        'Diana',
        'Nedqlkova',
        "0865555555",
        "https://www.linkedin.com/DNedqlkova",
        0,
        1,
        0
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        linked_account,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'PDubarova@abv.bg',
        '$2b$12$DBzLlYMtHMxeNUCTImIEqek66hcwIDbzFE6uC2Cvk.0eYd/t9LAiS',
        'Petq',
        'Dubarova',
        "0852222222",
        "https://www.linkedin.com/PDubarova",
        0,
        1,
        0
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        linked_account,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'EPelin@gmail.com',
        '$2b$12$oFQzRNQEXlPJhaIwppVqK.w/rnrRjs79uh8GNbNpng0V5NT97IdhC',
        'Elin',
        'Pelin',
        "0863333333",
        "https://www.linkedin.com/EPelin",
        0,
        1,
        0
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        linked_account,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'AGeorgiev@gmail.com',
        '$2b$12$ko3XWv9wTCyTVuE3XJ2SKuSbO8pNbmdTVKAQ6EJhcOfK5y9RmoU8q',
        'Alex',
        'Georgiev',
        "0842222222",
        "https://www.linkedin.com/AGeorgiev",
        0,
        0,
        0
    );
INSERT INTO poodle2.user (
        username,
        password,
        firstname,
        lastname,
        phone_number,
        linked_account,
        is_deactivated,
        is_teacher,
        is_admin
    )
VALUES (
        'CChaplin@gmail.com',
        '$2b$12$nVjUOh5V0qXcxzs8MtafSeq8MCHMAb0Jkp6//LKtLgI/eMI4a0CsO',
        'Charlie',
        'Chaplin',
        "0863333333",
        "https://www.linkedin.com/CChaplin",
        1,
        1,
        0
    );
INSERT INTO poodle2.courses (
        title,
        description,
        objective,
        owner,
        is_premium,
        is_deactivated,
        home_page
    )
VALUES (
        'OOP',
        'Learn what is OOP and how to use it!',
        'Learn Object Oriented Programing',
        'EPelin@gmail.com',
        0,
        0,
        'https://www.random.com/image/1'
    );
INSERT INTO poodle2.courses (
        title,
        description,
        objective,
        owner,
        is_premium,
        is_deactivated,
        home_page
    )
VALUES (
        'DSA',
        'First steps in DSA',
        'Undersand Trees and Algoritms',
        'PDubarova@abv.bg',
        1,
        0,
        'https://www.random.com/image/2'
    );
INSERT INTO poodle2.courses (
        title,
        description,
        objective,
        owner,
        is_premium,
        is_deactivated,
        home_page
    )
VALUES (
        'FastApi',
        'Learn the best web framework',
        'Master FastAPi',
        'CChaplin@gmail.com',
        1,
        1,
        'https://www.random.com/image/3'
    );
INSERT INTO poodle2.courses (
        title,
        description,
        objective,
        owner,
        is_premium,
        is_deactivated,
        home_page
    )
VALUES (
        'FastApi OAuth2',
        'Learn Authorization',
        'Master OAuth2',
        'EPelin@gmail.com',
        1,
        0,
        'https://www.random.com/image/4'
    );
INSERT INTO poodle2.courses (
        title,
        description,
        objective,
        owner,
        is_premium,
        is_deactivated,
        home_page
    )
VALUES (
        'Math101',
        'Math for 1st grade',
        'Learn addition and subtraction',
        'EPelin@gmail.com',
        0,
        0,
        'https://www.random.com/image/5'
    );
INSERT INTO poodle2.courses (
        title,
        description,
        objective,
        owner,
        is_premium,
        is_deactivated,
        home_page
    )
VALUES (
        'Math102',
        'Math for 2nd grade',
        'learn multiplication and division',
        'EPelin@gmail.com',
        0,
        0,
        'https://www.random.com/image/6'
    );
INSERT INTO poodle2.tags (tag_name)
VALUES ('Math');
INSERT INTO poodle2.tags (tag_name)
VALUES ('FastApi');
INSERT INTO poodle2.tags (tag_name)
VALUES ('OOP');
INSERT INTO poodle2.tags (tag_name)
VALUES ('DSA');
INSERT INTO poodle2.tags (tag_name)
VALUES ('Biology');
INSERT INTO poodle2.tags (tag_name)
VALUES ('Chemistry');
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM user
            WHERE user.username = 'PPetrov@abv.bg'
        ),
        (
            SELECT id
            FROM courses
            WHERE courses.title = 'OOP'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM user
            WHERE user.username = 'GAngelov@gmail.com'
        ),
        (
            SELECT id
            FROM courses
            WHERE courses.title = 'DSA'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM user
            WHERE user.username = 'DNedqlkova@abv.bg'
        ),
        (
            SELECT id
            FROM courses
            WHERE courses.title = 'FastApi'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM user
            WHERE user.username = 'PDubarova@abv.bg'
        ),
        (
            SELECT id
            FROM courses
            WHERE courses.title = 'FastApi OAuth2'
        ),
        (0)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM user
            WHERE user.username = 'EPelin@gmail.com'
        ),
        (
            SELECT id
            FROM courses
            WHERE courses.title = 'Math101'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM user
            WHERE user.username = 'CChaplin@gmail.com'
        ),
        (
            SELECT id
            FROM courses
            WHERE courses.title = 'Math102'
        ),
        (0)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE user.username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'DSA'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE user.username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE user.username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi OAuth2'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE user.username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'Math101'
        ),
        (1)
    );
INSERT INTO poodle2.user_has_courses(
        user_id,
        courses_id,
        approved
    )
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE user.username = 'DNedqlkova@abv.bg'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'OOP'
        ),
        (0)
    );
INSERT INTO poodle2.tags_has_courses(tags_id, courses_id)
VALUES(
        (
            SELECT id
            FROM poodle2.tags
            WHERE tags.tag_name = 'OOP'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'OOP'
        )
    );
INSERT INTO poodle2.tags_has_courses(tags_id, courses_id)
VALUES(
        (
            SELECT id
            FROM poodle2.tags
            WHERE tags.tag_name = 'Math'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'Math101'
        )
    );
INSERT INTO poodle2.tags_has_courses(tags_id, courses_id)
VALUES(
        (
            SELECT id
            FROM poodle2.tags
            WHERE tags.tag_name = 'Math'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'Math102'
        )
    );
INSERT INTO poodle2.tags_has_courses(tags_id, courses_id)
VALUES(
        (
            SELECT id
            FROM poodle2.tags
            WHERE tags.tag_name = 'FastApi'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi OAuth2'
        )
    );
INSERT INTO poodle2.tags_has_courses(tags_id, courses_id)
VALUES(
        (
            SELECT id
            FROM poodle2.tags
            WHERE tags.tag_name = 'DSA'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'DSA'
        )
    );
INSERT INTO poodle2.tags_has_courses(tags_id, courses_id)
VALUES(
        (
            SELECT id
            FROM tags
            WHERE tags.tag_name = 'FastApi'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Classes",
        "Creating classes",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'OOP'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Objects",
        "Creating objects",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'OOP'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Linear Data Stuctures",
        "Linked list",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'DSA'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Linear Data Stuctures",
        "Stack",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'DSA'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Linear Data Stuctures",
        "Queue",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'DSA'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Introduction",
        "FastApi presentation",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Importing",
        "FastApi Importing",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Other Frameworks",
        "FastApi Compare to other Frameworks",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "O Auth2",
        "Conception",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'FastApi OAuth2'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Matrix",
        "Basics",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'Math101'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Determinants",
        "Basics",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'Math101'
        )
    );
INSERT INTO poodle2.section(title, content, courses_id)
VALUES(
        "Integrals",
        "Basics and solutions",
        (
            SELECT id
            FROM poodle2.courses
            WHERE courses.title = 'Math102'
        )
    );
INSERT INTO poodle2.review(user_id, courses_id, rating, description)
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE username = 'PPetrov@abv.bg'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE title = 'OOP'
        ),
        5,
        "Best course ever"
    );
INSERT INTO poodle2.review(user_id, courses_id, rating, description)
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE username = 'GAngelov@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE title = 'DSA'
        ),
        3,
        "Not bad but could better"
    );
INSERT INTO poodle2.review(user_id, courses_id, rating, description)
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE username = 'DNedqlkova@abv.bg'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE title = 'FastApi'
        ),
        4,
        "it was enjoyable but needs to cover more"
    );
INSERT INTO poodle2.review(user_id, courses_id, rating, description)
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE title = 'FastApi'
        ),
        2,
        "slow paced"
    );
INSERT INTO poodle2.review(user_id, courses_id, rating, description)
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE title = 'FastApi OAuth2'
        ),
        3,
        "it was ok"
    );
INSERT INTO poodle2.review(user_id, courses_id, rating, description)
VALUES(
        (
            SELECT id
            FROM poodle2.user
            WHERE username = 'AGeorgiev@gmail.com'
        ),
        (
            SELECT id
            FROM poodle2.courses
            WHERE title = 'Math101  '
        ),
        5,
        "easy to understand, loved it!"
    );