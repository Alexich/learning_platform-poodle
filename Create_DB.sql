-- MySQL Workbench Forward Engineering
SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS,
  UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS,
  FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE,
  SQL_MODE = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema poodle2
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema poodle2
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `poodle2` DEFAULT CHARACTER SET latin1;
USE `poodle2`;
-- -----------------------------------------------------
-- Table `poodle2`.`courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`courses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `objective` VARCHAR(45) NOT NULL,
  `owner` VARCHAR(45) NOT NULL,
  `is_premium` TINYINT(4) NOT NULL,
  `is_deactivated` TINYINT(4) NOT NULL DEFAULT 0,
  `home_page` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC) VISIBLE
) ENGINE = InnoDB AUTO_INCREMENT = 29 DEFAULT CHARACTER SET = latin1;
-- -----------------------------------------------------
-- Table `poodle2`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`user` (
  `id` INT(45) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` LONGTEXT NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `phone_number` VARCHAR(45) NULL DEFAULT NULL,
  `linked_account` VARCHAR(45) NULL DEFAULT NULL,
  `is_deactivated` TINYINT(4) NOT NULL,
  `verified` TINYINT(4) NULL DEFAULT 0,
  `is_teacher` TINYINT(4) NOT NULL DEFAULT 0,
  `is_admin` TINYINT(4) NOT NULL DEFAULT 0,
  `premium_courses_left` INT(11) NOT NULL DEFAULT 5,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE
) ENGINE = InnoDB AUTO_INCREMENT = 130 DEFAULT CHARACTER SET = latin1;
-- -----------------------------------------------------
-- Table `poodle2`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`review` (
  `id` INT(45) NOT NULL AUTO_INCREMENT,
  `user_id` INT(45) NOT NULL,
  `courses_id` INT(11) NOT NULL,
  `rating` INT(11) NOT NULL,
  `description` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_has_courses1_courses1_idx` (`courses_id` ASC) VISIBLE,
  INDEX `fk_user_has_courses1_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_courses1_courses1` FOREIGN KEY (`courses_id`) REFERENCES `poodle2`.`courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_courses1_user1` FOREIGN KEY (`user_id`) REFERENCES `poodle2`.`user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 6 DEFAULT CHARACTER SET = latin1;
-- -----------------------------------------------------
-- Table `poodle2`.`section`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`section` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `courses_id` INT(11) NOT NULL,
  `content` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_section_courses1_idx` (`courses_id` ASC) VISIBLE,
  CONSTRAINT `fk_section_courses1` FOREIGN KEY (`courses_id`) REFERENCES `poodle2`.`courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 20 DEFAULT CHARACTER SET = latin1;
-- -----------------------------------------------------
-- Table `poodle2`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`tags` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tag_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 23 DEFAULT CHARACTER SET = latin1;
-- -----------------------------------------------------
-- Table `poodle2`.`tags_has_courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`tags_has_courses` (
  `courses_id` INT(11) NOT NULL,
  `tags_id` INT(11) NOT NULL,
  PRIMARY KEY (`courses_id`, `tags_id`),
  INDEX `fk_tags_has_courses_courses1_idx` (`courses_id` ASC) VISIBLE,
  INDEX `fk_tags_has_courses_tags1_idx` (`tags_id` ASC) VISIBLE,
  CONSTRAINT `fk_tags_has_courses_courses1` FOREIGN KEY (`courses_id`) REFERENCES `poodle2`.`courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_has_courses_tags1` FOREIGN KEY (`tags_id`) REFERENCES `poodle2`.`tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARACTER SET = latin1;
-- -----------------------------------------------------
-- Table `poodle2`.`user_has_courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `poodle2`.`user_has_courses` (
  `user_id` INT(45) NOT NULL,
  `courses_id` INT(11) NOT NULL,
  `approved` TINYINT(4) NOT NULL,
  PRIMARY KEY (`user_id`, `courses_id`),
  INDEX `fk_user_has_courses_courses1_idx` (`courses_id` ASC) VISIBLE,
  INDEX `fk_user_has_courses_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_courses_courses1` FOREIGN KEY (`courses_id`) REFERENCES `poodle2`.`courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_courses_user1` FOREIGN KEY (`user_id`) REFERENCES `poodle2`.`user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB DEFAULT CHARACTER SET = latin1;
SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;