import mysql.connector
from mysql.connector import Error
from data.data import read_query
try:
    connection = mysql.connector.connect(
        host='localhost',
        user='root',
        password='2580'
    )
except Error as e:
    print("Error connecting to MySQL server", e)


def createBD():
    cursor = connection.cursor()
    try:
        cursor.execute("CREATE DATABASE poodle2")
        print("Database created")
    except Error as e:
        pass

    connectionv2 = mysql.connector.connect(
        host='localhost',
        user='root',
        password='2580',
        database="poodle2"
    )
    cursor = connectionv2.cursor()
    try:
        cursor.execute("USE poodle2")
        with open('Create_DB.sql', 'r') as script_file:
            script = script_file.read()
            cursor.execute(script, multi=True)
    except Error as e:
        print("Error creating Relationships", e)


def add_data():
    if not _check_for_data():
        print('Add data to DB? Y/N')
        answer = input().lower()
        if answer == 'y':
            connectionv3 = mysql.connector.connect(
                host='localhost',
                user='root',
                password='2580',
                database="poodle2"
            )
            cursor = connectionv3.cursor()
            try:
                with open('add_data.sql', 'r') as data_file:
                    statements = data_file.read().split(';')
                    for statement in statements:
                        if statement.strip():  # Skip empty statements
                            cursor.execute(statement)
                    print("Data added")
                    connectionv3.commit()
            except Error as e:
                # print("Error adding data", e)
                pass


def _check_for_data():
    try:
        data = read_query('''SELECT * FROM poodle2.user''')
        return data
    except:
        return None
